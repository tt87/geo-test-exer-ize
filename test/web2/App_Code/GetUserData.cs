﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Security.Authentication;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;

public class GetUserData : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";
        context.Response.StatusCode = 200;
        var connection = DbConnect.PrepareConnection();
        int code = 200;
        var result= "";
        try
        {
            connection.Open();
            var user = User.Find(connection, context.Request);
            result = user.ToString();
        }
        catch (NpgsqlException ex)
        {
            code = 500;
            result = PrepareError(ex.Detail, code);
        }
        catch (AuthenticationException ex)
        {
            code = 401;
            result = PrepareError("Wrong login or password", code);
        }
        catch (Exception ex)
        {
            code = 400;
            result = PrepareError("System error", code);
        }
        finally
        {
            context.Response.Write(result);
            context.Response.StatusCode = code;
            connection.Close();
        }
    }

    public string PrepareError(string message, int code)
    {
        return JsonConvert.SerializeObject(new Dictionary<string, string> { { "ErrorMessage", message}, {"ErrorCode", code.ToString()} }, Formatting.Indented);
    }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}