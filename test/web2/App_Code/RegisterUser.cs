﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Web;
using System.Web.UI.WebControls.WebParts;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;

public class RegisterUser : IHttpHandler
{
    //todo need use with POST 
    public void ProcessRequest(HttpContext context)
    {
        var connection = DbConnect.PrepareConnection();
        context.Response.ContentType = "text/json";
        int code = 200;
        var result = "";
        try
        {
            connection.Open();
            var user = User.Insert(connection, context.Request);
            result = user.ToString();
        }
        catch (ArgumentException ex)
        {
            code = 400;
            result = PrepareError(ex.Message, code);
        }
        catch (NpgsqlException ex)
        {
            code = 500;
            result = PrepareError(ex.Message, code);
        }
        catch (Exception ex)
        {
            code = 500;
            result = PrepareError("System error", code);
        }
        finally
        {
            context.Response.StatusCode = code;
            context.Response.Write(result);
            connection.Close();
        }
    }

    public string PrepareError(string message, int code)
    {
        return JsonConvert.SerializeObject(new Dictionary<string, string> { { "ErrorMessage", message }, { "ErrorCode", code.ToString() } }, Formatting.Indented);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}