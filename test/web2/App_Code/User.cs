using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Authentication;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;

/// <summary>
/// It's some sort of Active Record pattern implementation.
/// </summary>
public class User
{
    private const string EmailPattern = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
    private static readonly PasswordHasher Hasher;

    public int Id { get; private set; }
    public string Name { get; private set; }
    public string Surname { get; private set; }
    public string Patronymic { get; private set; }
    public string Email { get; private set; }
    public int[] Groups { get; private set; }
    public string[] GroupNames { get; private set; }
    [JsonIgnore]
    public string Password { get; private set; }

    static User()
    {
        Hasher = new PasswordHasher();
    }

    public static User ParseInsertUserRequest(HttpRequest request)
    {
        return new User
        {
            Name = request["name"] ?? "",
            Surname = request["surname"] ?? "",
            Patronymic = request["patronymic"] ?? "",
            Email = ParseEmail(request["email"]),
            Password = Hash(request["password"]),
            Groups = ParseUserGroupIds(request["groups"])
        };
    }

    private static int[] ParseUserGroupIds(string s)
    {
        if (s == null)
            return new int[0];
        var arr = s.Split(',');
        int tmp;
        return arr.Any(x => int.TryParse(x, out tmp)) ? arr.Select(int.Parse).ToArray() : new int[0];
    }

    private static string ParseEmail(string s)
    {
        var isEmail = Regex.IsMatch(s, EmailPattern, RegexOptions.IgnoreCase);
        if (!isEmail)
            throw new ArgumentException("Wrong email format");
        return s.ToLowerInvariant();
    }

    private static string Hash(string s)
    {
        if (string.IsNullOrEmpty(s)) //todo restriction should be more strong
            throw new ArgumentException("Password should not be null or empty");
        return Hasher.HashPassword(s);
    }

    public static NpgsqlCommand PrepareInsertCommand(NpgsqlConnection connection, User user)
    {
        var result = new NpgsqlCommand("insert_user", connection) { CommandType = CommandType.StoredProcedure };
        result.Parameters.AddWithValue("pname", user.Name);
        result.Parameters.AddWithValue("psurname", user.Surname);
        result.Parameters.AddWithValue("ppatronymic", user.Patronymic);
        result.Parameters.AddWithValue("pemail", user.Email);
        result.Parameters.AddWithValue("ppassword", user.Password);
        return result;
    }

    private static NpgsqlCommand ParepareSaveUserGroupsCommand(NpgsqlConnection connection, User user)
    {
        var nGroups = user.Groups.Length;
        var userIds = Enumerable.Repeat(user.Id, nGroups).ToArray();
        var command = new NpgsqlCommand("insert_links", connection) { CommandType = CommandType.StoredProcedure };
        command.Parameters.AddWithValue("user_ids", NpgsqlDbType.Array | NpgsqlDbType.Integer, nGroups, userIds);
        command.Parameters.AddWithValue("group_ids", NpgsqlDbType.Array | NpgsqlDbType.Integer, nGroups, user.Groups);
        return command;
    }

    public static NpgsqlCommand PrepareFindUserCommand(NpgsqlConnection connection, string email)
    {
        var result = new NpgsqlCommand("find_user", connection) { CommandType = CommandType.StoredProcedure };
        result.Parameters.AddWithValue("pemail", email);
        return result;
    }

    public static User Insert(NpgsqlConnection connection, HttpRequest request)
    {
        var user = ParseInsertUserRequest(request);
        var insertUserCommand = PrepareInsertCommand(connection, user);
        user.Id = (int) insertUserCommand.ExecuteScalar();
        var saveUserGroupsCommand = ParepareSaveUserGroupsCommand(connection, user);
        saveUserGroupsCommand.ExecuteNonQuery();
        return user;
    }

    public static User Find(NpgsqlConnection conection, HttpRequest request)
    {
        var password = request["password"];
        var command = PrepareFindUserCommand(conection, request["email"]);
        var reader = command.ExecuteReader();
        if (!reader.Read())
        {
            throw new AuthenticationException();
        }
        var hashedPass = reader.GetString("password");
        var res = Hasher.VerifyHashedPassword(hashedPass, password);
        if (res == PasswordVerificationResult.Failed)
        {
            throw new AuthenticationException();
        }
        return GetUserFromReader(reader);
    }

    public static User GetUserFromReader(NpgsqlDataReader reader)
    {
        return new User
        {
            Email = reader.GetString("email"),
            Password = reader.GetString("password"),
            Name = reader.GetString("name"),
            Surname = reader.GetString("surname"),
            Patronymic = reader.GetString("patronymic"),
            Id = reader.GetInt("user_id"),
            GroupNames = reader.GetStrings("groups")
        };
    }

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }
    
}


public static class Converters
{

    public static string GetString(this NpgsqlDataReader reader, string rowName)
    {
        return reader[rowName] as string ?? "";
    }

    public static string[] GetStrings(this NpgsqlDataReader reader, string rowName)
    {
        return reader[rowName] as string[] ?? new string[0];
    }

    public static int GetInt(this NpgsqlDataReader reader, string rowName)
    {
        return reader[rowName] as int? ?? 0; 
    }

}