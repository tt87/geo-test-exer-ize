﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Чуров Тимофей Николаевич</h3>
    <address>
        <abbr title="Phone">Телефон:</abbr>
        +7 911 184-94-95
    </address>
    <address>
        <strong>email:</strong>   <a href="mailto:tchurovtim@gmail.com">tchurovtim@gmail.com</a><br />
    </address>
</asp:Content>
