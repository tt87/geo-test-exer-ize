﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <style type="text/css">
        .resultlinks {
            list-style-type: none;
        }

        .tasklist li {
            margin-bottom: 10px;
        }

        .tasklist>li>p {
            border-bottom: 1px dashed;
            display: inline-block;
            color: #337ab7;
        }


    </style>
   <h2>Тестовые задания Тимофея Чурова</h2>
    <ol class="tasklist">
        <li><p title="Кликните для просмотра уловий первого задания">Первое задание:</p>
            <pre style="display: none">
                1. Написать класс map на Javascript для генерации карты OpenStreetMap
                2. Инициализация карты должна выполняться функцией map.init()
                3. Использовать jQuery и Leaflet
                4. Карта должна быть на всю страницу (без рамок)
                5. При клике правой кнопкой мыши по карте должно открываться меню с пунктами:
                    - Пункт 1
                    - Пункт 2
                    - Пункт 3
                6. Пункты меню должны читаться из файла json (структуру придумать)
                7. При потере фокуса меню должно закрываться
                8. Внутри перетаскиваемого окна должны быть:
                    - одна кнопка, которая приближает карту к г. Санкт-Петербург
                    - вторая кнопка, которая должна конвертировать карту (только карту) в формат PNG и сохранять (или открывать в новой вкладке)
                9. Пункт 3 должен открывать перетаскиваемое окно размером 800х600 (добавить шапку. Кнопку закрытия окна)
                10. Остальные пункты должны просто закрывать меню
            </pre>
            
            <ol class="resultlinks">
                <li><a href="https://bitbucket.org/tt87/geo-test-exer-ize/wiki/Task1">Инструкция к первому заданию</a></li>
                <li> <a href="task1.html" target="_blank">Страница с картой</a></li>
            </ol>
        </li>
        <li>
            <p title="Кликните для просмотра условий второго задания">Второе задание:</p>
             <pre style="display: none">
                1. Придумать структуру БД для хранения информации о пользователях, группах пользователей и написать процедуры для добавления пользователей
                2. Пользователь может входить в несколько групп
                3. СУБД выбираете сами (предпочтительно MS SQL, PostgreSQL)
                4. Сделать страницу и написать хендлер (ashx + c#) который добавляет пользователя в БД из пунктов выше (все селекты и инсерты – исключительно через процедуры в БД)
                5. Сделать страницу и написать хендлер (ashx + c#) который проверяет доступ пользователя из пунктов выше и выводит на эту же страницу всю информацию о нём (все селекты и инсерты – исключительно через процедуры в БД)
            </pre>
            <ol class="resultlinks">
                <li><a href="https://bitbucket.org/tt87/geo-test-exer-ize/wiki/Task2">Инструкция ко второму заданию</a></li>
                <li> <a id="registerlink" href="RegisterUser.ashx?email=user@foo.bar&name=alex&patronymic=nguid&surname=Lopatin&password=12345&groups=1,3,4" target="_blank">Хендлер 1: Регистрация пользователя</a></li>
                <li> <a id="getuserdatalink" href="GetUserData.ashx?email=user@foo.bar&password=12345" target="_blank">Хендлер 2: Получение данных пользователя</a></li>
            </ol>
        </li>
        <li>
             <a href="https://bitbucket.org/tt87/geo-test-exer-ize/src" target="_blank">Исходные коды заданий</a>
        </li>
        <li>
            <p title="Кликните для просмотра">Блок-схемы</p>
            <pre style="display: none">
                Блок-схемы отлично подходят для описания последовательных вычислительных алгоиртмов и при обучении школьников 
                и студентов младших курсов профильных вузов. 
                На мой взгляд, в современных реалиях разработки программного обеспечения, таких как объектно-ориентированное, 
                сервисно-ориентированное и функционального программирование, многопоточность и широкое использование сторонних 
                библиотек и фрейворков, блок-схемы не могут быть применимы для описания бизнесс процессов.
                Вместо блок-схем я сформировал диаграмму классов второго тестового задания:
                <img src="Content/Graph.png" alt="Диаграмма классов" style="width: 100%">
            </pre>
            
        </li>
    </ol>
    
    <script type="text/javascript" src="Scripts/jquery-2.1.4.min.js"></script>
    <script type="text/javascript">
        $(".tasklist p").each(function() {
                this.onclick = function() {
                    $(this).parent().find("pre").toggle();
                };
            }
        );
       
        var name = Math.random().toString(36).replace(/[^a-z]+/g, '');
        var email = name + "@foo.bar";
        var patronymic = Math.random().toString(36).replace(/[^a-z]+/g, '');
        var surname = Math.random().toString(36).replace(/[^a-z]+/g, '');
        var password = Math.random().toString(36).replace(/[^a-z]+/g, '');
        var groups = "1,3,4";

        $("#registerlink").attr("href", "RegisterUser.ashx?email=" + email + "&name=" + name + "&patronymic=" + patronymic + "&surname=" + surname + "&password=" + password + "&groups=1,3,4");
        $("#getuserdatalink").attr("href", "GetUserData.ashx?email=" + email + "&password=" + password);
    </script>
</asp:Content>
