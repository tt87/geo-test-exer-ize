﻿<%@ WebHandler Language="C#" Class="FileHandler" %>

using System;
using System.Web;
using System.IO;

public class FileHandler : IHttpHandler
{
    private static readonly string SavePathDir = Path.GetTempPath();
    
    public void ProcessRequest (HttpContext context)
    {
        string savePath = SavePathDir + context.Request.Params["FileName"];
        
        context.Response.ClearHeaders();
        context.Response.ContentType = "image/png";
        
        var ms = new MemoryStream(File.ReadAllBytes(savePath));
        context.Response.AddHeader("Content-Length", ms.Length.ToString());
        ms.WriteTo(context.Response.OutputStream);
        ms.Close();
        
        context.Response.Flush();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}