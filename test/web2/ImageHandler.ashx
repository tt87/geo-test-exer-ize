﻿<%@ WebHandler Language="C#" Class="ImageHandler" %>

using System;
using System.Web;
using System.IO;
using Newtonsoft.Json;

public class ImageHandler : IHttpHandler {

    private static readonly string SavePathDir = Path.GetTempPath();
     private static readonly string FileName = "tmp" + Guid.NewGuid() + ".png";
     private static readonly string SavePath = SavePathDir + FileName;
    
    public void ProcessRequest (HttpContext context) {
        string pngString;
        using (var reader = new StreamReader(context.Request.InputStream))
        {
            pngString = reader.ReadToEnd().Replace("\"","");            
        }
        var pngBytes = Convert.FromBase64String(pngString);
        File.WriteAllBytes(SavePath, pngBytes);

        context.Response.ContentType = "text";
        context.Response.StatusCode = 200;
        context.Response.Write(JsonConvert.SerializeObject(new System.Collections.Generic.Dictionary<string, string> { { "fileName", FileName }}, Formatting.Indented));
        context.Response.Flush();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
}