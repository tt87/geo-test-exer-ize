﻿var Map = function () { };
Map.prototype.mapObject = null;

Map.prototype.init = function (lat, lon) {
   
    this.mapObject = L.map("map",
    {
        contextmenu: true,
        contextmenuWidth: 140
    }).setView([lat, lon], 13);

    L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6IjZjNmRjNzk3ZmE2MTcwOTEwMGY0MzU3YjUzOWFmNWZhIn0.Y8bhBaUMqFiPrDRW9hieoQ", {
        maxZoom: 18,
        attribution: "Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, " +
            "<a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, " +
            "Imagery © <a href=\"http://mapbox.com\">Mapbox</a>",
        id: "mapbox.streets"
    }).addTo(this.mapObject);

    var that = this;
    that.openDialog = that.openDialog.bind(that);

    $("#spb").click(function () {
        that.mapObject.setView([59.94366, 30.30579], 15);
        $("#dialog").dialog("close");
    });

    $("#save").click(function () {
        leafletImage(that.mapObject, that.makeImage);
        $("#dialog").dialog("close");
    });

    $.getJSON("./dist/menu.json")
        .done(function(json) {
            for (var i = 0; i < json.arr.length; i++) {
                if (i === 2) {
                    that.mapObject.contextmenu.addItem({ text: json.arr[i].text, callback: that.openDialog });
                } else {
                    that.mapObject.contextmenu.addItem({ text: json.arr[i].text });
                }
            }
        })
        .fail(function(q, s, error) {
            console.log(q + " " + s + " " + error);
        });
};


Map.prototype.openDialog = function () {
    $("#dialog").dialog({ width: 800, height: 600 });
};


Map.prototype.makeImage = function(err, canvas) {
    var dataUrl = canvas.toDataURL("image/png");
    var imgData = dataUrl.replace(/^data:image\/(png|jpg);base64,/, "");

    $.ajax({
        url: "ImageHandler.ashx",
        dataType: "json",
        data: imgData,
        type: "POST",
        cache: false,
        success: function(data) {
            window.open("FileHandler.ashx?FileName=" + data.fileName);
        }
    });
}